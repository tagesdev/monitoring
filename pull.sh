#!/bin/bash
rm -rf cache
helm pull victoria-metrics-k8s-stack --untar --destination cache/. --version 0.19.0 --repo "https://victoriametrics.github.io/helm-charts/"
rm -rf bundle/monitoring
helm template monitoring cache/victoria-metrics-k8s-stack -f cache/victoria-metrics-k8s-stack/values.yaml -f values.yaml --output-dir bundle/. --namespace monitoring

